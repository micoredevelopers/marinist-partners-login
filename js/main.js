const validation = (input) => {
	if (input.value === '') {
		input.classList.add('error');
	} else {
		input.classList.remove('error');
	}
};

$('.form-box').on('submit', function (e) {
	e.preventDefault();
	let dataSend = {};
	let $formInputs = $(this).find('input');
	let dataUrl = $(this).attr('action');

	$formInputs.each((index, item) => {
		validation(item);
		dataSend[$(item).attr('id')] = $(item).val();
	});

	if (!$formInputs.hasClass('error')) {
		$.ajax({
			url: dataUrl,
			method: "POST",
			data: dataSend,
		})
			.done(function (res) {
				if (res.status === 'success') {
					$('#modalSuccess').modal('show');
				}
			})
			.fail(function (res) {
				console.log(res);
			})
	}
});
